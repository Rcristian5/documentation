# Estructura de un archivo de angular

1.- La estructura para los componentes, módulos, servicios, etc. deben según la siguiente forma: importación, decorador, clase. Separadas siempre por 2 líneas en blanco, para notar esa diferencia. p.e.:


> Componente

```javascript
import { Component } from '@angular/core'


@Component({
	selector: 'app'
})


export class MyApp {}
```

> Modulo

```javascript
import { Module } from '@angular/core'


@NgModule({
	declarations: [],
	imports: [],
	providers: [],
	bootstrap: []
})


export class AppModule { }
```

> Servicio

```javascript
import { Injectable } from '@angular/core'


@Injectable()


export class MyService {}
```
