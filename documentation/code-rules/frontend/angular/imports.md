# Importaciones

1.- Las importaciones dentro de los archivos de typescript deben siempre estar el principio, y ordenados según su procedencia en, importacion de angular, importaciones de bibliotecas de terceros e importaciones de nuestro propio código. p.e.:

```javascript
import { Component } from '@angular/core'
import { Http } from '@angular/http'
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";
import { EmployeeServices } from '../../services/employee'
```

2.- Debe existir un espacio entre las llaves de la importación y los paquetes a importar, así como entre los mismos paquetes a importar. p.e.:

```javascript
import { Component, Module } from '@angular/core'
```
