# Código homogéneo en las clases de TypeScript

1.- La declaración de las variables globales de una clases deben ser las primeras líneas de código.
```javascript
class MyClass {
	a: number = 1
	b: string = 'hello'
}
```


2.- Colocar el constructor solo para inyectar las dependencias que necesite la clase y de manera privada, colocar el signo de pesos '$' como prefijo al nombrar a la dependencia, para identificar a lo largo del codigo que es una dependencia inyectada.
```javascript
class MyClass {
	a: number = 1


	constructor ( private $http: Http ) {}
}
```

3.- Si el constructor tiene más de una dependencia, colocarlo en lista, una debajo de otra.
```javascript
class MyClass {
	a: number = 1


	constructor (
		private $http: Http,
		private $dep1: Dep1,
		private $dep2: Dep2
	) {}
}
```

4.- El constructor no debe contener lógica, en su caso utilizar el método ngOnInit.
```javascript
class MyClass {
	a: number = 1


	constructor (
		private $http: Http,
		private $dep1: Dep1,
		private $dep2: Dep2
	) {}



	ngOnInit (): void {
		//code
	}
}
```


5.- Al declarar algun método/función declarar el tipo de variable que acepta en sus parámetros y el retorno de la función.
```javascript
class MyClass {
	a: number = 1


	constructor (
		private $http: Http,
		private $dep1: Dep1,
		private $dep2: Dep2
	) {}



	myMethod ( _age: number ): number {
		return 10
	}
}
```


6.- Espaciar con 3 líneas entre cada método/función declarado, incluyendo la declaración de las variables globales y constructor.
```javascript
class MyClass {
	a: number = 1


	myMethod (): void {
		//code
	}



	myMethod2 ( _age: number ): number {
		return _age
	}
}
```
