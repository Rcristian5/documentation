# Condicionales

 1.- Siempre se deben escribir triples iguales para comparar. p.e.:

```javascript
// igual a ...
if ( x === 1 ) { ... }

// diferente de
if ( y !== 1 ) { ... }
```

 2.- Siempre colocar los parentesis de las codicionales aunque esta solo ejecute una sola linea de codigo. p.e.:

```javascript
if ( x ) {
	console.log(x)
}
```