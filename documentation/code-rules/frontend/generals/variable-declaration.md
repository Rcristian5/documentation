# Declaración de variables

Para todo el código generado se deben seguir las siguientes reglas.

1.- Las variables se declaran siempre al principio de cada método o archivo (Usar la menor cantidad de variables globales).
```javascript
function myFunction () {
	var x = null
	let y = []
	const z = null

	foreach( x in y ) { ... }
}
```

2.- Todas las variables se deben declarar usando la palabra reservada `var` o `let` o `const`, de preferencia `let` y `const` para ES6 y el menor numero de variables a `this` para las clases en typescript. p.e.:
```javascript
// variable inicializada
var x = null
let y = null
const z = null
```

3.- Todas las variables deben ser inicializadas con un null en caso de ser objetos, cadenas o listas si no se necesitan inicializar objetos vacíos o listas vacías y con un 0 si son de tipo entero o flotante. Para typescript definir el tipo de variable. p.e.:
```javascript
// tipo string
var myString = null;
// tipo object
let myObject = {};
// tipo lista en typescript
let myList: Array<number> = [];
// tipo numero en typescrit
let myInt: number = 0;
```

4.- Todas las declaraciones de variables deben estar en camel-case iniciando la primer letra con minúscula. p.e.:
```javascript
// declaración correcta de una variable
let myPrettyVar = null;

// declaraciones incorrectas de una variable
let my_pretty_var = null;

let my_prettyVar = null;

let MyPrettyVar = null;
```

5.- El nombre de todas las clases creadas en typescript o ES6 deben ser en UpperCamelCase, p.e.:
```javascript
class MyClass { ... }
```
