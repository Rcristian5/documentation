# Espaciados, argumentos y parametros

Esta estandarización se debe seguir en todo momento para que los desarrolladores puedan tener un código homogéneo en todo el desarrollo.

1.- Debe existir un espacio antes y después de un igual, de un igual-igual-igual, de un menor que y de un mayor que. p.e.:

```javascript
// antes y después de un igual
var x = 0
let y = 0
const z = 0

// antes y después de un igual-igual-igual
if ( x === 12 ) { ... }

// antes y después de un menor que
if ( y <= 12 ) { ... }

// antes y después de un mayor que
if ( z >= 12 ) { ... }
```

2.- Siempre debe existir un espacio antes y uno después de cada parametro en un método o condicional, en caso de usar typescript agregar un espacio despues de los dos puntos al definir el tipo de variable. p.e.:

```javascript
// declarando un método/función
function myFunction ( _param1, _param2, ..., _paramN ) { ... }

// declaración de una función arrow
const myFunction = ( _param1, _param2, ..., _paramN ) => { ... }

// en typescript
myFunction ( _param1: string, _param2: number, ..., _paramN: boolean ) { ... }

// ejecutando un método/función
myFunction( param1, param2, ..., paramN )

// en un condicional
if ( conditionalExpression ) { ... }

while( conditionalExpression ) { ... }

for ( initialization ; conditionalExpression ; afterExecution ) { ... }

do { ... } while( conditionalExpression )
```

3.- Siempre debe existir un espacio entre la declaración del condicional o del método/función y la llave de su contenido de instrucciones.p.e.:
```javascript
// declaración de un método/función
function myFunction () { ... }

// declaración de un condicional y ciclos
while ( conditionalExpression ) { ... }
```

4.- Si un método/función no necesita parametros, los paréntesis deben permanecer sin espacios, sin embardo debe existir un espacio entre el nombre del método/función y el primer paréntesis aunque el método/función necesite parametros o no. p.e.:

```javascript
// declaración de método/función
function myFunction () { ... }
```

5.- Los parametros de un método siempre deben iniciar con un guión bajo ( _ ) para poder identificar entre las variables locales y los parametros. p.e.:

```javascript
// declaración de método
function myFunction ( _param1, _param2, ..., _paramN ) { ... }

// typescript
myFunction ( _param1: string, _param2: number, ..., _paramN: boolean ) { ... }
```

6.- Para las funciones arrow, se colocaran paréntesis solo si lo requiere como por ejemplo al no contar con algun parametro o contar con más de un parámetro, para una mejor sintaxis. Y colocar las llaves que definan el cuerpo de la función solo si lo requieren como en el caso de contar con más de una línea de código que debe ejecutar la función.

```javascript
// si no lleva argumentos
const myFunction = () => { ... }

// si lleva un sólo argumento
const myFunction = _param1 => return _param1 === 10

// si lleva N argumentos
const myFunction = ( _param1, _param2, ..., _paramN ) => { ... }
```

7.- Cuando un método se ejecuta, si necesita parametros, estos se deben colocar con un espacio antes del primer parametro, también un espacio después de la coma y un espacio después del último parametro. p.e.:

```javascript
// ejecución de un método
myFunction( param1, param2, ..., paramN )
```
8.- Se podrá evitar el uso de los puntos y coma “;” al terminar una sentencia, para tener una codigo más limpio


```javascript
// ejecución de un método
myFunction( param1, param2, ..., paramN )
const a = 10
this.b = 'Hello'
```
