# Scaffolding para un proyecto de Angular

![Con titulo](../../../assets/screenshot-scaffolding-angular-1.png "titulo")
![Con titulo](../../../assets/screenshot-scaffolding-angular-2.png "titulo")

Se propone un scaffolding para mantener estructurado los archivos del proyecto. A continuación se menciona las secciones principales.

## Raiz del proyecto

Sobre la raíz del proyecto podremos encontrar numerosos archivos.

* `webpack.config.js`
* `tscofnig.json`
* `package.json`
* `.angular-cli.json`
* ...

Los archivos que se encuentran en esta sección, son archivos de configuración para el proyecto de angular.

También podemos encontrar algunas carpetas, por ejemplo.

* `e2e/` ( Test ).
* `node_modules/` ( Modulos de la aplicacion, instalar con npm y registradas en el package.json ).
* `src/` ( Ahí se encontrará todo el código de nuestra aplicación ).
* `.git` ( Control de versiones ).

## Folder `src/`

Esta carpeta es la parte más importante para nosotros, ya que en ella pasaremos la mayoría de nuestro tiempo al trabajar con la aplicación, en ella trabajaremos para crear la lógica de negocio de la aplicación, ahí pondremos nuestro código.

A este nivel del proyecto podemos encontrar archivos y carpetas de la aplicación, para la  configuración e inicialización de la misma.

* `app/` ( Nuestro código de lógica de negocio )
* `assets/` ( Archivos estáticos de nuestra aplicación )
* `environments/` ( Configuración para los entornos de desarrollo )
* `styles/` ( Estilos globales de la aplicación  )
* `templates/` ( Maquetas reutilizables o apartadas )
* `favicon.ico`
* `index.html`
* `main.ts`
* `polyfills.ts`
* `styles.css`
* ...


## Folder `app/`

En esta carpeta, es donde desarrollaremos todo nuestro código de la aplicación, dividido en categorías (sub carpetas) dependiendo del la funcionalidad del código, como por ejemplo.

* `components/`
* `directives/`
* `interfaces/`
* `modules/`
* `pipes/`
* `services/`

> Los folders son categorizados de esa manera por la lógica que se incorpora de el frameworks de angular.

En el folder `components/` colocaremos todos los componentes que se creen para la aplicación, ya que el número de componentes puede crecer demasiado, se subcategorizará los componentes en: `views/` que son aquellos componentes que corresponden a alguna vista y que usualmente se colocan en las rutas de nuestra aplicación y `elements/` que serán aquellos componentes que podremos reutilizar o encapsulan lógica de algún comportamiento.

Un componente debe tener siempre un archivo `.ts`, que es donde inicializamos nuestro componente y opcionalmente un template (extraído en un archivo o embebido en el .ts) y una hoja de estilos (extraído en un archivo o embebido en el .ts), para mantener una homogeneidad nosotros utilizaremos siempre las extracción del template y estilos en un archivo separado. Ya que tenemos 3 archivos que conforman a un solo componente, agrupamos a estos en una carpeta (nombrada con el nombre del componente) para mantenerlos agrupados, para el nombre de los archivos ( `.ts`, `.scss` y `.pug` ), colocamos también el mismo nombre del componente, como se muestra en el ejemplo al principio de este manual.

> Si el nombre del componente contiene más de una sola palabra, estas se deben unir con guiones medios “-” tanto para el nombre de la carpeta como para la de los archivos (donde solo cambiará la extensión).


El folder `modules/` también contendrá una categorización en: `config/` que serán aquellos módulos que dividen la aplicación y se utiliza para configurar las declaraciones, importaciones, etc. y ‘routes/’ serán los módulos que se utilizan exclusivamente para declarar rutas de la aplicación.


Para los folders `directives/`, `interfaces/`, `pipes` y `services/`, se colocarán los archivos a nivel raíz del folder.


## Nomenclaturas para nombres

Así como se mencionó en la nomenclatura para nombrar los componentes y sus folders que lo contienen. Esa misma lógica se utiliza para nombrar todas los folders del proyecto y archivos.

Si el nombre de un archivo o folder contiene más una palabra, estas deben unirse por medio de guiones medios “-”.
