# Documentación

Este repositorio tiene como único objetivo agregar documentación sobre scaffolding de proyectos y reglamentación para los desarrolladores, estas documentación ayudarán a escribir un código mucho más legible, más fácil de entender y mucho más limpio. Esto ayudará también a que futuros desarrolladores que entren a los proyectos tengan una curva de aprendizaje más rápida ya que al saber como se debe codificar y estructurar los proyectos, lograrán entender los procesos de códigos mucho más fácil.

## Scaffolding

* [Proyecto con Angular](documentation/scaffolding/frontend/angular.md)


## Estandarización de código

### Frontend

#### _Generales_

* [Espaciados, argumentos y parametros](documentation/code-rules/frontend/generals/spaces-and-parameters.md)
* [Declaración de variables y clases](documentation/code-rules/frontend/generals/variable-declaration.md)
* [Loops e iteraciones](documentation/code-rules/frontend/generals/loops-and-iterations.md)
* [Condicionales](documentation/code-rules/frontend/generals/conditional.md)

#### _Angular_

* [Clases](documentation/code-rules/frontend/angular/class.md)
* [Importaciones](documentation/code-rules/frontend/angular/imports.md)
* [Estructura de un archivo de angular](documentation/code-rules/frontend/angular/struct-file-angular.md)
* [TypeScript best practices](http://definitelytyped.org/guides/best-practices.html)
